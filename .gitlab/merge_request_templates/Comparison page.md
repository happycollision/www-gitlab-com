### Description

Create comparison page for: GitLab vs <Competitor>.

Closes <link to issue>

### Preview URL


/label ~"comparison page"
