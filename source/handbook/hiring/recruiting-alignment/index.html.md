---
layout: markdown_page
title: "Recruiting Alignment"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiter and Coordinator Alignment by Hiring Manager

| Sales                    | Recruiter       | Coordinator                                |
|--------------------------|-----------------|--------------------------------------------|
| Richard Pidgeon          | Nadia Vatalidis | Kike Adio                                  |
| Chad Malchow             | Kelly Murdock   | Kike Adio                                  |
| Michael Alessio          | Nadia Vatalidis | Kike Adio |
| Francis Aquino           | Kelly Murdock   | Kike Adio |
| Paul Almeida             | Kelly Murdock   | Kike Adio |
| Kristen Lawrence         | Kelly Murdock   | Kike Adio |
| Leslie Blanchard         | Kelly Murdock   | Kike Adio |

| Marketing                                      | Recruiter       | Coordinator                                |
|------------------------------------------------|-----------------|--------------------------------------------|
| Erica Lindberg                                 | Jacie Zoerb     | Kike Adio |
| Ashish Kuthiala                                | Jacie Zoerb     | Kike Adio |
| LJ Banks                                       | Jacie Zoerb     | Kike Adio |
| Alex Turner                                    | Jacie Zoerb     | Kike Adio |
| Elsje Smart                                    | Nadia Vatalidis | Kike Adio |
| Director of Field Marketing (to be hired, Leslie Blanchard as interim) | Jacie Zoerb     | Kike Adio |
| Director of Marketing Operations (to be hired, LJ Banks as interim) | Jacie Zoerb     | Kike Adio |
| David Planella                                 | Jacie Zoerb     | Kike Adio |

| Engineering           | Recruiter                                               | Coordinator |
|-----------------------|---------------------------------------------------------|-------------|
| Tim Zallmann          | Eva Petreska (to be hired, Nadia Vatalidis as interim)  | Emily Mowry |
| Sarrah Vesselov       | Eva Petreska (to be hired, Chloe Whitestone as interim) | Emily Mowry |
| Kathy Wang            | Steve Pestorich                                         | Emily Mowry |
| Tommy Morgan          | Trust Ogor                                              | Emily Mowry |
| Mek Stittri           | Eva Petreska (to be hired, Nadia Vatalidis as interim)  | Emily Mowry |
| Dalia Havens          | Trust Ogor                                              | Emily Mowry |
| Tom Cooney            | Nadia Vatalidis                                         | Emily Mowry |
| Gerir Lopez-Fernandez | Steve Pestorich                                         | Emily Mowry |

| Product           | Recruiter                       | Coordinator |
|-------------------|---------------------------------|-------------|
| Mark Pundsack     | Nadia Vatalidis/Steve Pestorich | Emily Mowry |
| Job van der Voort | Nadia Vatalidis                 | Emily Mowry |

| Other         | Recruiter                       | Coordinator                                |
|---------------|---------------------------------|--------------------------------------------|
| Paul Machle   | Jacie Zoerb/Nadia Vatalidis     | Kike Adio |
| Barbie Brewer | Jacie Zoerb/Nadia Vatalidis     | Kike Adio |
| Brandon Jung  | Kelly Murdock                   | Kike Adio |
| Meltano       | Steve Pestorich/Nadia Vatalidis | Kike Adio |

## Sourcer Alignment by Division and Location

| Product               | Region            | Sourcer                | Estimated % |
|-----------------------|-------------------|------------------------|-------------|
| Sales & Marketing     | Americas/Anywhere | Stephanie Garza        | 100%        |
| Sales & Marketing     | EMEA & APAC       | Anastasia Pshegodskaya | 25%         |
| Engineering Backend   | Anywhere          | Zsuzsanna Kovacs       | 100%        |
| Engineering General   | Anywhere          | Anastasia Pshegodskaya | 50%         |
| Other/Director+ Roles | Anywhere          | Anastasia Pshegodskaya | 25%         |
