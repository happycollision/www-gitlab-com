---
layout: markdown_page
title: "Legal"
---

## On this page
{:.no_toc}

- TOC
{:toc}
 
## Communication
 
### Issue Trackers
If you need to request legal services, including third party contract review, third party license review, legal advice or guidance, please submit all requests through the Legal private issue tracker, by submitting an [email](mailto:legal@gitlab.com).
 
Please be sure to include sufficient detail regarding your request, including time-sensitive deadlines, relevant documents, and background information necessary to respond.  Through the Legal private issue tracker, you will be updated regarding the status of your request through the Service Desk feature.  The Executive Team will have full access to the [Legal issue tracker](https://gitlab.com/gitlab-legal/legal-issue-tracker).
 
### Sales Contracts

If a customer requests to edit our standard Subscription or Services Agreements, please follow the process outlined in the [Sales Handbook](https://about.gitlab.com/handbook/sales/#process-for-agreement-terms-negotiations-when-applicable).

### Chat Channel
Feel free to use the `#legal` chat channel in Slack for general legal questions that don't seem appropriate for the issue tracker or internal email correspondence.  Slack is not to be used for anything that is considered confidential or seeking legal advice.
 
## Contract Templates
 
* [Contracts](/handbook/contracts/)
* Non-Disclosure Agreement (NDA) (WIP)

### Master Vendor Agreement
All vendors and suppliers doing business with GitLab will require a contract. If the vendor/supplier does not provide an agreement, then GitLab's Master Vendor Agreement can be used. See Vendor Agreement. If using this Master Vendor Agreement, please provide any changes to the template to Legal for revieww. All agreements must be reviewed and approved by Legal before signing.
 
 
## Legal Team Processes
 
* [Signing Legal Documents](/handbook/signing-legal-documents/)
* [Authorization Matrix](/handbook/finance/authorization-matrix/)
* [Approval for Vendor Contracts](/handbook/finance/procure-to-pay)
 
## Company Policies
 
[Anti-harassment](/handbook/anti-harassment/)
 
 
## General Topics

Frequently Asked Questions (WIP)
 
 
## Other Pages Related to Legal
 
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information)
* [General Guidelines](/handbook/general-guidelines/)
* [Terms](/terms/)