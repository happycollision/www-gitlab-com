---
layout: markdown_page
title: "Distribution Team Open Work Day"
---

## On this page

- TOC
{:toc}

## Common links

* [Distribution Team Handbook](/handbook/engineering/dev-backend/distribution/)
* [Distribution Team Infrastructure](/handbook/engineering/dev-backend/distribution/infrastructure)

## Introduction

We are a [product driven company](/handbook/product#how-to-work-aswith-product). This means that the
Product team is responsible for prioritizing items that engineering will be
working on. Further more, we optimize for [velocity over predictability](/handbook/engineering#velocity-over-predictability) which means
that we will always have overscheduled milestones.

Engineers can still talk with their Engineering Manager and put forward items
that they find important, but that doesn't always mean that those items will
be scheduled for delivery.

In our [handbook general guideliness](/handbook/general-guidelines/) we state:

> We recognize that inspiration is perishable, so if you’re enthusiastic about something that generates great results in relatively little time feel free to work on that.

Given that inspiration is perishable and it needs some time to grow,
Distribution team is assigning one day a month to work on an item that team
member finds important.

## Open Work Day

To ensure that we use company time wisely, there are a few pointers worth noting:

* It is not required to ask for permission to work on something
  * It is expected that you will present to your team why the item you worked on was important to you and explain the thought process behind it. This can be presented during the next Distribution weekly call
* There is no set day for open work day
  * You do not need to have completed all your deliverables in the cycle, but you must ensure that your main deliverable is completed
* Item you are working on must be GitLab related
* Choose an item that won't turn into a multi-month project
  * Wanted to do that refactor that someone left behind? Great!
  * Want to fix that item that is always at the bottom of your todo list? Super!
  * You need that small feature that improves your workflow but it just keeps missing scheduling? Awesome!
  * Want to rewrite part of the application? Uh, that does not sound good; Talk to your EM!
* Assign the issue to yourself and apply the label "distribution-OWD" to *both* the issue and MR
