---
layout: markdown_page
title: "Code Review"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

Code reviews are mandatory for every merge request, you should get familiar and follow our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html).

These guidelines also describe who would need to review, approve and merge your merge request.

## Reviewer

All GitLab engineers can, and are encouraged to, perform code review on merge requests of colleagues and community contributors. If you want to review merge requests, you can wait until someone assigns you one, but you are also more than welcome to browse the list of open merge requests and leave any feedback or questions you may have.

To let other engineers know that you are interested in performing code reviews, you can add this to your [team page entry](/company/team/) by editing [`data/team.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml).

You can find someone to review your own merge requests by looking on the [team page](/company/team/), or on the list of [GitLab Engineering Projects](/handbook/engineering/projects/), both of which are fed by `data/team.yml`.

You can also help community contributors get their merge requests ready, by becoming a [Merge Request Coach](/roles/merge-request-coach/).

Note that while all engineers can review all merge requests, the ability to _accept_ merge requests is restricted to maintainers.

## Maintainer

Maintainers are GitLab engineers who are experts at code review, know the GitLab product and code base very well, and are empowered to accept merge requests in one or several [GitLab Engineering Projects](/handbook/engineering/projects/).

Every project has at least one maintainer, but most have multiple, and some projects (like GitLab CE and GitLab EE) have separate maintainers for frontend and backend.

Great engineers are often also great reviewers, but code review is a skill in and of itself, and not every engineer, no matter their seniority, will have had the same opportunities to hone that skill. It's also important to note that a big part of being a good maintainer comes from knowing the existing product and code base extremely well, which lets them spot inconsistencies, edge cases, or non-obvious interactions with other features that would otherwise be missed easily.

To protect and ensure the quality of the code base and the product as a whole, people become maintainers only once they have convincingly demonstrated that their reviewing skills are at a comparable level to those of existing maintainers.

As with regular reviewers, maintainers can be found on the [team page](/company/team/), or on the list of [GitLab Engineering Projects](/handbook/engineering/projects/).

### How to become a maintainer

**This applies specifically to backend maintainers. Other areas (frontend, database, etc.) may have separate processes.**

As a reviewer, a great way to improve your reviewing skills is to participate in MRs. Add your review notes, pass them on to maintainers, and follow the conversation until the MR is closed. If a comment doesn't make sense to you, ask the commenter to explain further. If you missed something in your review, figure out why you didn't see it, and note it down for next time.

Reviewers need to make the case for their own maintainership status. We
have two guidelines for maintainership, but no concrete rules:

1. [Junior engineers][junior-engineers] should be focused on becoming
intermediate engineers over attempting to become maintainers.
    1. In general, the further along in the career someone is, the more
       we expect them to be capable of becoming a maintainer.
2. Most people should aim for being at GitLab for a year before applying
   to be a maintainer. This is to get a good feel for the codebase,
   expertise in one or more domains, and deep understanding of our
   coding standards. For Staff levels and higher, this may be reduced.

When:
 - the MR's they've reviewed consistently make it through maintainer review without significant additionally required changes;
 - the MR's they've written consistently make it through reviewer and maintainer review without significant required changes;
they can:

* create an MR to add the maintainership to their team page entry
* explain in the MR body why they are ready to take on that responsibility
* use specific examples of recent "maintainer-level" reviews that they have performed
* assign the MR to your manager and to the existing maintainers of the relevant product (GitLab CE, GitLab EE, etc) and area (backend, frontend, etc)

The MR's should not reflect only small changes to the code base, but also architectural ones and ones that create a full feature.

If the existing maintainers do not have significant objections, and if at least half of them agree that the reviewer is indeed ready, we've got ourselves a new maintainer!

The existing maintainers will also raise any areas for growth on the merge request. If there are many gaps, the reviewer will need to address these before asking for reconsideration.

If you'd like to work towards becoming a maintainer, discuss it in your regular 1:1 meetings with your manager. They will help you to identify areas to work on before following the process above.

[junior-engineers]: /about/handbook/career-development/junior-engineers/
