---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2018-10-31.

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 368   | 100%        |
| Based in the US                           | 211   | 57.34%      |
| Based in the UK                           | 22    | 5.98%       |
| Based in the Netherlands                  | 14    | 3.80%       |
| Based in Other Countries                  | 121   | 32.88%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 368   | 100%        |
| Men                                       | 287   | 83.67%      |
| Women                                     | 71    | 23.62%      |
| Other Gender Identities                   | 0     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 31    | 100%        |
| Men in Leadership                         | 25    | 80.65%      |
| Women in Leadership                       | 6     | 19.35%      |
| Other Gender Identities                   | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 151   | 100%        |
| Men in Development                        | 135   | 89.31%      |
| Women in Development                      | 16    | 11.03%      |
| Other Gender Identities                   | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        |211    | 100%        |
| Asian                                     | 16    | 7.58%       |
| Black or African American                 | 4     | 1.90%       |
| Hispanic or Latino                        | 11    | 5.21%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.47%       |
| Two or More Races                         | 9     | 4.27%       |
| White                                     | 115   | 54.50%      |
| Unreported                                | 55    | 26.07%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 50    | 100%        |
| Asian                                     | 4     | 8.00%       |
| Black or African American                 | 1     | 2.17%       |
| Hispanic or Latino                        | 3     | 6.00%       |
  Two or More Races                         | 3     | 6.00%       |
| White                                     | 27    | 54.00%      |
| Unreported                                | 12    | 24.00%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 25    | 100%        |
| Asian                                     | 3     | 12.00%      |
| Native Hawaiian or Other Pacific Islander | 1     | 4.00%       |
| White                                     | 11    | 44.00%      |
| Unreported                                | 10    | 40.00%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 368   | 100%        |
| Asian                                     | 31    | 8.42%       |
| Black or African American                 | 9     | 2.45%       |
| Hispanic or Latino                        | 18    | 4.89%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.27%       |
| Two or More Races                         | 11    | 2.99%       |
| White                                     | 191   | 51.90%      |
| Unreported                                | 107   | 29.08%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 151   | 100%        |
| Asian                                     | 12    | 7.95%       |
| Black or African American                 | 3     | 1.99%       |
| Hispanic or Latino                        | 9     | 5.96%       |
| Two or More Races                         | 5     | 3.31%       |
| White                                     | 78    | 51.66%      |
| Unreported                                | 44    | 29.14%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 31    | 100%        |
| Asian                                     | 3     | 9.68%       |
| Hispanic or Latino                        | 1     | 3.23%       |
  Native Hawaiian or Other Pacific Islander | 1     | 3.23%       |
| White                                     | 12    | 38.71%      |
| Unreported                                | 14    | 45.16%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 368   | 100%        |
| 18-24                                     | 16    | 4.35%       |
| 25-29                                     | 87    | 23.64%      |
| 30-34                                     | 102   | 27.72%      |
| 35-39                                     | 62    | 16.85%      |
| 40-49                                     | 66    | 17.93%      |
| 50-59                                     | 32    | 8.70%       |
| 60+                                       | 2     | 0.54%       |
| Unreported                                | 1     | 0.27%       |
