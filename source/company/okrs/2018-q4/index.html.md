---
layout: markdown_page
title: "2018 Q4 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV according to plan. Sales prospects. Marketing achieves the SCLAU plan. Measure pipe-to-spend for all marketing activities.

* VPE
  * Support: Improve Support expectations and documentation for Proof of Concept and Trial prospects, including temporary expanded Support focus for top 10 prospects (defined by Sales).
      * Amer East: 95% SLA for Premium/Ultimate/Gitlab.com customers and 95% CSAT for all customers
      * Amer West: 95% SLA for Premium/Ultimate/Gitlab.com customers and 95% CSAT for all customers
      * APAC: 95% SLA for Premium/Ultimate/Gitlab.com customers and 95% CSAT for all customers
      * EMEA: 95% SLA for Premium/Ultimate/Gitlab.com customers and 95% CSAT for all customers
* CMO: Marketing achieves the SCLAU plan. 8 SCLAU per SDR per month, content 2x QoQ, website information architecture helps 50% QoQ.
  * Product Marketing: Complete Information Architecture for customer facing web site pages and add 50 product comparison pages.
  * Product Marketing: Deliver 15 enablement sessions each to sales and xDR teams - 30 total sessions.
  * Product Marketing: Target 6 industry analyst reports to include GitLab coverage.
  * Content Marketing: Increase sessions on content marketing blog posts 5% MoM.
      * October: 55,583 sessions
      * November: 58,362 sessions
      * December: 61,280 sessions
* CMO: Measure pipe-to-spend for all marketing activities. Content team based on it, link meaningful conversations to pipe, double down on what works.
* VP Alliances: Establish differentiated relationships with Major Clouds. GitLab as a Service LoI on a major cloud, 500+ leads through joint marketing, GTM and Partner agreement in place tihe a large private cloud provider.
* VP Alliances: establish GitLab as DevOps tool for CNCF.  Three keynote/speaking engagements at CNCF events and 1000+ leads from K8s partners
* CFO: Use data to drive business decisions
  * Director of Business Operations: Create scope for User Journey that is documented and mapped to key data sources.
  * Director of Business Operations: Top of funnel process and metrics defined and aligned with bottom of funnel.
    * Data & Analytics: Able to define and calculate Customer Count, MRR and ARR by Customer, Churn by Cohort, Reason for Churn
  * Director of Business Operations: Looker Explores generated for Customer Support, PeopleOps, GitLab.com Event Data, Marketing Data
    * Data & Analytics: Single Data Lake for all raw and transformed company data - migration to Snowflake compelte with DevOps workflow in place, GitLab.com production data extracted and modelled
    * Data & Analytics: Configuration and Business processes documented with integrity tests that sync back with upstream source (SFDC), dbt docs deployed
* Product: Listen to and prioritize top feature requests from customers through sales / customer success. 10 features prioritized.

### CEO: Popular next generation product. Finish 2018 vision of entire DevOps lifecycle. GitLab.com is ready for mission critical applications. All installations have a graph of DevOps score vs. releases per year.

* VPE: Make GitLab.com ready for mission critical customer workloads
  * Frontend:
    * Spend no more than 15 point of our [error budget]: 0/15
    * Convert a Vue app to use GraphQL as a side-by-side feature with user opt-in
    * Create, Plan:
      * Spend no more than 15 point of our [error budget]: 0/15
      * 3 charts in gitlab-ui
    * Distribution, Monitor and Packaging:
      * Spend no more than 15 point of our [error budget]: 0/15
      * Rewrite and integrate 4 CSS components that align with our design system into GitLab
  * Dev Backend:
    * Adopt in collaboration with Product a consistent process for prioritizing
      work across all backend teams (x/13)
    * Perform 3 iterations on [retrospective](/handbook/engineering/workflow/#retrospective) process to be more effective for Engineering (1/3)
    * Gitaly:
      * Spend no more than 15 point of our [error budget]: 0/15
      * Deliver first iteration of Object Deduplication work
    * Gitter:
      * Spend no more than 15 point of our [error budget]: 0/15
      * Shut down billing and embeds on `apps-xx`
    * Plan:
      * Spend no more than 15 point of our [error budget]: 0/15
      * Complete [phase 1 of preparedness for Elasticsearch on GitLab.com]
    * Create:
      * Spend no more than 15 point of our [error budget]: 0/15
    * Manage:
      * Spend no more than 15 point of our [error budget]: 15/15 [#536](https://gitlab.com/gitlab-com/gl-infra/production/issues/536)
    * Geo:
      * Spend no more than 15 point of our [error budget]: 0/15
      * Complete all issues in Phase 1 of the DR/HA plan working with the Production Team ()
    * Distribution:
      * Spend no more than 15 point of our [error budget]: 0/15
      * [Automate library upgrades](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/194) across distributed projects
  * Ops Backend:
    * Drive the implementation of a throughput dashboard for each development team and conduct a training for each: x/13 dashboards, x/1 practice training, x/1 group training
    * Spend no more than 15 point of our [error budget]: 0/15
    * Configure: Merge smaller changes more frequently: &gt; 10 average MRs merged per week, &lt; 100 average lines added, &lt; average 7 files changed
    * Secure:
      * Spend no more than 15 point of our [error budget]: 0/15
      * Merge smaller changes more frequently: &gt; 4 average MRs merged per week
    * [Philippe Lafoucrière](https://about.gitlab.com/company/team/#plafoucriere): Benchmark 3 new security tools for integration in the [Security Products](https://about.gitlab.com/handbook/engineering/ops-backend/secure/#security_products)
    * [Philippe Lafoucrière](https://about.gitlab.com/company/team/#plafoucriere): One CFP (Call For Proposals) accepted for a Tech conference
    * Monitor:
      * Spend no more than 15 point of our [error budget]: 0/15
      * Merge smaller changes more frequently: &gt; 6 average MRs merged per week
    * [Kamil](https://about.gitlab.com/company/team/#ayufanpl):
      * Improve scalability: Reduce CI database footprint as part of [Ensure CI/CD can scale in future](https://gitlab.com/gitlab-org/gitlab-ce/issues/46499) with [Use BuildMetadata to store build configuration in JSONB form](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/21499).
      * Continuous Deployment: Help with building CD features needed to continously deploy GitLab.com by closing 5 [issues related to Object Storage](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name=Object%20Storage) that are either ~"technical debt" or ~"bug"
    * Spend no more than 15 point of our [error budget]: 15/15 [#5375](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5375)
    * CI/CD: Merge smaller changes more frequently: >10 average MRs merged per week
  * Infrastructure: Make GitLab.com ready for mission critical workloads
    * Spend no more than 15 point of our [error budget]: 0/15
    * Deliver streamlined and cleaned-up Infrastructure Handbook
    * SAE: Minimize GitLab.com's MTTR
      * Deliver observable availability improvements in database backup/restore/verification and replication
      * Launch `production` queue management for changes, incidents, deltas and hotspots
    * SRE: Maximize GitLab.com's MTBF
      * Deliver phase 1 DR and HA plan for GitLab.com
      * Implement roadmap and milestone planning with tracking of velocity
    * Andrew: Observable Availability
      * Deliver General Service Metrics and Alerting, Phase 2: error budgets dashboardI and attribution, alert actionability metrics, and MTTD metrics
      * Deliver [Distributed Tracing working in GDK](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/4808)
      * Deliver DDoS ELK alerting, documentation and training
    * Delivery: Streamline releases
      * Create release pipelines
      * Create a CD blueprint for 2019
  * Quality:
     * Complete [Review Apps for CE and EE with GitLab QA running]: 100% ()
     * Complete phase 1 & 2 of [Addressing database discrepancy for our on-prem customers] : 100% ()
  * Support
    * Amer East: Develop at least 2 Premium service process improvements focused on improved coordination with TAMS, Premium customer ticket reduction efforts, and create premium ticket reports that give us more clarity of needs within our Premium customer base.
    * Amer West: Create a *Support Engineer - GitLab.com* Bootcamp more tightly aligned with Production Engineering
    * APAC: TBD process improvement (e.g. SLA performance, Customer Experience, or Employee Development)
    * EMEA: Enable support team to create 30 updates to docs to help capture knowledge and improve customer self service. Track contributions and views to measure 'self service score' (support edited doc visitors / customers opening tickets).
  * UX:
    * Create a seamless cluster experience. Identify 3 additional ways users can create and manage their clusters. Draft a proposal for an MVC that considers product, technical, and user requirements. Push for scheduling and implementation of the first iteration of group and instance clusters in Q4.
    * [Complete a Competitive analysis of version control tools for designers (Abstract, Invision, Figma, etc.). Identify ways GitLab can leverage Open Source and our own tools to assist designers using GitLab.](https://gitlab.com/gitlab-org/gitlab-ce/issues/51654)
    * UX Research
      * [Establish a GitLab beta group and create a standardized process for running beta studies with users. Conduct 1 beta study with users to test and iterate on the process.](https://gitlab.com/gitlab-org/ux-research/issues/100)
  * Security:
      * Deprecate support for TLS 1.0 and 1.1 on GitLab.com: Y%
      * Complete and document 12 month roadmap for ZTN rollout: Y%
    * Security Operations:
      * Document at least 5 runbooks for logging sources: X/5, Y%
      * Security and Priority labels on 50 backlogged security issues: X/50, Y%
    * Application Security:
      * Complete at least 2 cycles of security release process: X/2, Y%
      * Conduct at least 3 application security reviews: X/3, Y%
      * Public HackerOne bounty program by December 15: X%
    * Compliance:
      * Document current procedures for each of the 14 ISO Operations Security Domain: X/14, Y%
      * Conduct gap analysis of at least one GitLab.com subnet's access controls: X/1, Y%
    * Abuse:
      * Port existing 'janitor' project to Rails w/ 100% feature parity: X/9, Y%
      * Support at least 2 external requests per month: X/6, Y%
* Product:
  * Significantly improve navigation of the documentation. Ship global navigation. Improve version navigation. Revamp [first page of the EE docs](https://docs.gitlab.com/ee/README.html).
  * Move vision forward every release. Prioritize at least one vision issue for each stage in every release.
  * Every new feature ships with usage metrics on day 1, without negatively impacting velocity.
* CMO:
  * Corporate marketing: Help our customers evangelize GitLab. 3 customer or user talk submissions accepted for DevOps related events.
  * Corporate marketing: Drive brand consistency across all events. Company-level messaging and positioning incorporated into pre-event training, company-level messaging and positioning reflected in all event collateral and signage.
  * Corporate marketing: Increase share of voice. 20% lift in web and twitter traffic during event activity.

### CEO: Great team. ELO score per interviewer, executive dashboards for all key results, train director group.

* CMO: Hire missing directors. Operations, Corporate, maybe Field.
* VPE: Craft an impactful iteration to improve our career development framework and train the team
* VPE: Source 20 candidates by Oct 31 and hire 2 Engineering Directors: 100% (20/20) sourced, hired 0/2 directors
  * Frontend:
    * Scale process to handle incoming amount ofapplications and hire 2 engineering
      managers and 10 engineers: X hired (X%)
  * Dev Backend:
    * Gitaly:
      * Source 10 candidates by October 15 and hire 1 developer: X sourced (X%),
        X hired (X%)
    * Gitter:
      * Create hiring process for fullstack developer
    * Plan:
      * Source 25 candidates (at least 5 by direct manager contact) by
        October 15 and hire 2 developers: X sourced (X%), X hired (X%)
    * Create:
      * Source 25 candidates (at least 5 by direct manager contact) by
        October 15 and hire 2 developers: 25 sourced (100%), X hired (X%)
      * Get 10 MRs merged into Go projects by team members without pre-existing
        Go experience: X merged (X%)
    * Manage:
      * Source 25 candidates (at least 5 by direct manager contact) by
        October 15 and hire 2 developers: X sourced (X%), X hired (X%)
      * Team to deliver 10 topic-specific knowledge sharing sessions
        (“201s”) by the end of Q4 (X completed)
      * Geo:
        * At least one Geo team member to advance to Geo Maintainer ()
    * Distribution
      * Source 25 candidates (at least 5 by direct manager contact) by
      October 15 and hire 2 developers: X sourced (X%), X hired (X%)
    * Stan:
      * [Reduce baseline memory usage in Rails by 30%](https://gitlab.com/gitlab-org/gitlab-ce/issues/49702)
      * [Reduce runtime memory usage in Sidekiq for top 5 workers by 30%](https://gitlab.com/gitlab-org/gitlab-ce/issues/49703)
      * Resolve 3 P1 Tier 1 customer issues
  * Infrastructure: Source 10 candidates by Oct 15 and hire 1 SRE (APAC) manager: X sourced (X%) X hired (X%)
    * SAE: Source 10 candidates by Oct 15 and hire 1 DBRE: X sourced (X%) X hired (X%)
    * SRE: Source 25 candidates by Oct 15 and hire 2 SREs: X sourced (X%) X hired (X%)
  * Ops Backend:
    * Hire 2 engineering managers (1 manager for Release, 1 manager for Secure): Hired X (X%)
    * Configure: Complete 10 introductory calls with prospective candidates by Nov 15 and hire 2 developers: 0 prospects (0%), hired 0 (0%)
    * Monitor: Source 30 candidates (at least 5 by direct manager contact) by November 15 and hire 3 developers: 0 sourced (0%), hired 0 (0%)
    * Secure: Source 75 candidates and hire 5 developers: X sourced (X%), X hired (X%)
    * CI/CD: Complete 10 introductory calls with prospective candidates by Nov 15 and hire 2 developers: 0 prospects (0%), hired 0 (0%)
  * Quality:
    * Source 100 candidates (at least 5 by direct manager contact) by
      October 15 and hire 2 test automation engineers: X sourced (X%), X hired (X%)
  * Security:
    * Source 100 candidates (at least 5 by direct manager contact) by
      October 15 and hire 3 security engineers: X sourced (X%), X hired (X%)
  * Support: Source 30 candidates by October 15 and hire 1 APAC Manager
    * Amer East: Source 30 candidates by October 15 and hire 1 Support Agent
    * Amer West: Source 30 candidates by October 15 and hire 1 Support Engineer
    * APAC: Source 30 candidates by October 15 and hire 1 Support Engineer
  * UX:
    * Source 50 candidates by October 15 (at least 10 by direct manager contact) and hire 2 UX Designers and 1 UX Manager: X sourced (X%), X hired (X%)
  * VP Alliances: Scale team to interact with our key partners. Hire Content Manager and Alliance Manager
  * CFO: Improve processes and compliance
    * Legal: Implement a new contracting process to roll out with the vendor management process.
    * Legal: Update global stock option program to ensure global compliance.
  * CFO: Create plan for preparing the Company for IPO
  * CFO: Improve financial performance
    * FinOps: Annual planning process complete with company wide benchmarks and 100% of functions modeled to enable scenario analysis.
    * FinOps: Headcount planning linked to requisition process that provides for recruiting and hiring manager visibility against plan assumptions.
  * CCO:  Improve Recruiting and People Ops Metrics, including relevant benchmarketing data
    * Recruiting: Hiring and Diversity stats and goals defined by functional group
    * Recruiting: Rent Index Evaluation by functional group
    * Recruiting: Single Source of truth for Recruiting data, ownership and progress. If we can get this down to 2 sources in Q4, that will be a win.
    * PeopleOps: Added clarity to the turnover data and industry benchmarketing. Attempt to get benchmark data for remote companies.
  * CCO: Continue on the next iteration of Compensation
    * PeopleOps: Review and revise benchmarks to meet our current Compensation Philosophy
    * PeopleOps: Explore and decide on revising the compensation philosophy that supports hiring and retaining top talent, and have the plan in place to move forward.
    * PeopleOps: Review and revise Rent Indexes to align with the compensation philosophy.
  * CCO: Continue to improve Career Development and Feedback
    * Redesign the experience factor model to focus on supporting development and conduct trainings.
    * Define Cross-Functional and Functional Group specifc VP and Director Criteria
    * Launch an employee engagement survey that results in at least 85% participation and 3 themes to work on as a leadership team.
  * CCO: Improve Recruiting efficiency and effectiveness to build a solid foundation to scale:
    * Clearly define and standardize process, roles, responsibilities, and SLA’s
    * Improve continuity and accountability through recruiter/sourcer/coordinator alignment
    * Launch candidate survey in Greenhouse
    * Implement and integrate new background check provider
    * Enhance candidate communication and templates (invites, updates, decline notices)
    * Optimize Greenhouse and provide refresher training to hiring managers
    * Support Headcount planning process driven by Finance to ensure one source of truth and process for hiring plan and recruiter capacity
    * Continue work on ELO score for interviews. First interation complete by mid November.
  * Product: Hire 2 Directors of Product

[error budget]: /handbook/engineering/#error-budgets
[phase 1 of preparedness for Elasticsearch on GitLab.com]: https://gitlab.com/groups/gitlab-org/-/epics/429
[Review Apps for CE and EE with GitLab QA running]: https://gitlab.com/groups/gitlab-org/-/epics/265
[Cross-browser and mobile browser test coverage]: https://gitlab.com/gitlab-org/quality/team-tasks/issues/45
[Addressing database discrepancy for our on-prem customers]: https://gitlab.com/gitlab-org/gitlab-ce/issues/51438
