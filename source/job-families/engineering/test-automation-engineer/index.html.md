---
layout: job_family_page
title: "Test Automation Engineer"
---

GitLab is looking for a motivated and experienced engineer to help grow our test automation efforts across the entire GitLab ecosystem. 
This is a key position with a new and growing team, so your efforts will have a noticeable impact to both the company and product. 
In addition to the requirements below, successful candidates will demonstrate a passion for high quality software, 
strong engineering principles, and methodical problem solving skills.

## Responsibilities

- Expand our existing test automation framework and test coverage.
- Develop new tests and tools for our GitLab.com frontend, backend APIs and services, and low-level systems like geo replication, 
CI/CD, and load balancing.
- Work with the product team and other development teams to understand how new features should be tested, 
and then engage them in contributing automated tests.
- Identify and drive adoption of best practices in code health, testing, testability, and maintainability. 
You should know about clean code and the test pyramid, and champion these concepts.
- Strive for the fastest feedback possible. Test parallelization should be a top priority. 
You see distributed systems as a core challenge of good test automation infrastructure.
- Configure automated tests to execute reliably and efficiently in CI/CD environments.
- Track and communicate test results in a timely, effective, and automated manner.

## Requirements

- Strong experience developing in Ruby (this is a strict requirement)
- Strong experience using Git
- Experience using test automation tools like Capybara, Watir, Selenium
- Relevant work experience in software development and/or test automation
- Experience working with Docker containers
- Experience with AWS or Kubernetes
- Experience with Continuous Integration systems (e.g., Jenkins, Travis, GitLab)



## Junior Test Automation Engineer

1 or more years of software engineering experience in Test Automation, Test Tooling and Infrastructure, or Development Deployment Operations.

* **Responsibilities**
  *  Implement basic test automation and continuous integration given designs and help from other team members.
  *  Maintain existing test automation framework.
  *  Maintain our CI system.
* **Planning & Organization**
  * Organize and complete structured assignments.
* **Independence & Initiative**
  * Work under general direction on problems of limited scope; use discretion to complete tasks.
  

## Intermediate Test Automation Engineer  
  
3 or more years of software engineering experience in Test Automation, Test Tooling and Infrastructure, or Development Deployment Operations.
  
* **Responsibilities** (extends the responsibilities of the previous level)
  * Implement test automation for new features with little guidance. 
  * Occasionally lead test automation efforts on new features.
  * Participate in test plan discussion.
  * Participate in architectural discussions.
  * Participate in design reviews with product management and engineering teams.
  * Maintain test infrastructure stability in production and non-production environments.
* **Planning & Organization**
  * Establish deadlines and approach for completing some assignments; demonstrates project management skills as part of larger team.
* **Independence & Initiative**
  * Work on problems of moderate scope; exercises judgment and independently identifies next steps.
  
## Senior Test Automation Engineer

7 or more years of software engineering experience in Test Automation, Test Tooling and Infrastructure, or Development Deployment Operations.


* **Responsibilities** (extends the responsibilities of the previous level)
  * Lead test automation implementation on new features. Create test plans for new features and steer the team to ensure test coverage based on the test plan.
  * Identify test gaps and prioritize adding coverage based on areas of risk.
  * Lead development of new tooling and infrastructure.
  * Provide guidance on testing approach for new feature development.
  * Implement new automation framework features with little guidance.
  * Provide input to test the security and scalability of the product.
  * Recommend build, test and deployment process improvements.
  * Recommend new test automation tools, and processes.
  * Take ownership of test failures and ensure that our CI system is reliable.
  * Mentor other engineers.
  * Occasionally contribute to the company blog.
* **Planning & Organization**
  * Independently and regularly manage project schedules ensuring objectives are aligned with team/department goals.
* **Independence & Initiative**
  * Work on problems of diverse scope requiring independent evaluation of identifiable factors; recommend new approaches to resolve problems.
  
## Staff Test Automation Engineer

10 or more years of software engineering experience in Test Automation, Test Tooling and Infrastructure, or Development Deployment Operations.

* **Responsibilities** (extends the responsibilities of the previous level)
  * Lead test automation infrastructure implementation across multiple product areas.
  * Regularly lead discussions on architectural improvements of test tooling and infrastructure.
  * Regularly consulted on testing approach for new feature development.
  * Analyze engineering metrics and make suggestions to improve engineering processes and velocity.
  * Participate in customer calls and take part in Engineering outreach.
  * Regularly review new content from the department added to the company blog.
* **Planning & Organization**
  * Define and independently manages multiple projects within the department.
* **Independence & Initiative**
  * Work on complex department issues that have an impact on the company's bottom-line. Able to create new methods for obtaining results.   
  
  
## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute screening call with our Recruiting team
- Candidates will be invited to complete a Technical assignment
- Next, candidates will then be invited to schedule a 1 hour technical interview with a Senior Test Automation Engineer
- Next, candidates will be invited to schedule a 45 minute first interview with the Engineering Manager, Quality.
- Candidates will be invited to schedule a third 45 minute interview with our VP of Engineering
- Finally, candidates may be invited to schedule a 50 minute interview with our CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
